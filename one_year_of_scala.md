# One Year of Writing Scala in Production

As of 2022 Jan 1st, I have officially worked at [paidy](https://paidy.com/) for
a year. In case you don't know, at paidy we are neck-deep in functional
programming, Scala is our main language of choice and we have a few services
written in Rust. Being a Java programmer for about 5-6 years, writing Scala was
really a steep learning curve. I already knew the basic syntax of the language
and already grasped most of the language features like algebraic data types
with sealed traits and case classes, pattern matching, and even implicits.
However, at paidy, we employ a style of functional programming that is more
commonly referred to as pure functional programming.

### What is Pure Functional Programming?

Without going into much details, pure functional programming is just a
programming style that enforces strict referential transparency on your code.
Referential transparency just means we treat everything as pure values that
don't have side effects. An integer is a value, it doesn't fire a rocket, it
simply just represents a bunch of bits. Similary, in pure functional
programming we treat functions that might potentially trigger a side effect as
values. This gives us the benefit of substituting these values easily.

## Pure Functional Programming with cats-effect
is it difficult?
learning curve?

## Type Safety
is it worth it?


