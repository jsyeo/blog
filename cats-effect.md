Random notes from cats effect

`Concurrent[F]`

runs things in parallel

`ContextShift[F]`

context shift between thread pool

`Resource[F[_], A]`
- something to manage the life time managed value `A`

- it has an `use` method that takes a function from `A => F[B]` and returns the
  effect type `F[B]`
- even if the function is cancelled, it will still do clean up of the resource

`Sync`
- delay that takes a thunk which is a side effect

tagless
`F[_]` means something is parameterised over the effect type

`Applicative`
- Along with `import cats.syntax.applicative._` gives you `Foo.pure[F]`

`Functor`
- Something that can be mapped over
- `import cats.syntax.functor._` to get `foo.map(...)` when `foo: F` and there's an implicit instance of `Functor[F]`.

`FlatMap`
- Along with `import cats.syntax.flatmap._` gives you `.flatMap` and operators like `>>`

`Option`
- gives you `.some`

`Traverse`
- gives you `.traverse`. Use it whenever you need to convert `F[G[_]]` to `G[F[_]]`

`Reader` monad
- A context for a type. E.g. `Reader[A, B]` gives you a context with `A` and returns a `B`
- Gives you a form of dependency injection

`EitherT`
- A bifunctor(?) that is basically the equivalent of `F[Either[A, B]]`
- Use `EitherT.rightT` to lift a value of type `B` into `EitherT[F, A, B]`
